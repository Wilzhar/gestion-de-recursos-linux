#!/usr/bin/python3
#-*- coding:utf-8 -*- 

import subproceso
import re
import sys
import graficos

def calcular_uso_cpu():	
    """ Metodo que me calcula el porcentaje de memoria RAM usada

		Returns
		-------
		float
			porcentaje de memoria RAM usada
	"""
    p = subproceso.output_proceso("top | head -3 | tail -1")
    info_cpu = str(re.sub(" +", " ", str(p)))
    
    info_cpu = info_cpu.split(" ")
    names = ["Por el usuario", "Por el sistema", "Inactivo"]
    datos_cpu = [float(info_cpu[1].replace(",", ".")), float(info_cpu[3].replace(",", ".")), float(info_cpu[7].replace(",", "."))]

    return graficos.generar_grafico_pastel("pie_chart_2", "Uso de CPU", names, datos_cpu)

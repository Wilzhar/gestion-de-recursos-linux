import subproceso
import re
import math
import graficos

def porc_llenado():
  
  estructura = """
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawAnnotations);

    function drawAnnotations() {

      var data = google.visualization.arrayToDataTable([
        ['porcentaje', 'Ocupado', 'Libre', { role: 'style' } ],
      """
  p = subproceso.ejecutar_subproceso("df -h | grep '/dev/'")

  lines = p[0]

  datos = ""
  
  for l in lines :
    l = re.sub( " +"," ",l )    
    info = l.split(" ")
    filled = int(info[4][:-1])
    empty = 100 - filled
    datos = datos+"[\""+str(info[0])+"\","+str(filled)+","+str(empty)+",''],"
    
  datos = datos[:-1] 
          
  estructura+= datos +"""      ]);
    
        var options = {
            title:'Porcentaje de llenado de las particiones',
            isStacked: 'percent',
            height: 300,
            legend: {position: 'top', maxLines: 3},
            vAxis: {
              minValue: 0,
              ticks: [0, .3, .6, .9, 1]
            }
          };

        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
        chart.draw(data, options);
      }
    </script>
    """
    
   
  return estructura 
    
def processors_load():
  estructura = """
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawAnnotations);

    function drawAnnotations() {

      var data = google.visualization.arrayToDataTable([  
        ['Procesador',""" 
   
  data = subproceso.output_proceso("uptime")
  data = re.sub( " +", " ",data) #Reemplazar espacios en blanco por 1 solo espacio en blanco
  data = data.split(" ")
  loads = data[-3:] #Toma de la posicion 7 a la 9 (No incluye la 10)
  load_1min = loads[0][:-1] #Quita la coma del final
  load_5min = loads[1][:-1] #Quita la coma del final
  
  loads[0] = load_1min
  loads[1] = load_5min
  info = ""
  np_max = 0 #Numero maximo de pocesadores usados
  for l in loads:
    l = l.replace(",",".")
    nptemp = math.ceil(float(l))
    if nptemp > np_max:
      np_max = nptemp
  for n in range(np_max):
    info += "'CPU"+str(n)+"',"
  info+="{ role: 'style' } ],"
  momento = ["Utimo min","Ultimos 5 min", "Ultimos 15 min"]
  
  for m in range(3): #para los momentos 1min, 5min y 15min
    info = info+"['"+str(momento[m])+"'"
    n_processors = loads[m].split(",")[0]
    left = loads[m].split(",")[1]
    
    np_unused = np_max - int(n_processors) - 1 #n procesadores sin usar = max - usados menos otro que se usa a medias pero cuenta como 1.
    for p in range(int(n_processors)):
      info+=",100"
    info += ","+str(left)+str(",0"*np_unused)+",''],"
  info = info[:-1] 
        
  estructura+= info +"""     ]);
    
        var options = {
          title:'Carga en los nucleos del Procesador',
          isStacked: true,
          height: 300,
          legend: {position: 'top', maxLines: 3},
          vAxis: {minValue: 0}
        };

        var chart = new google.visualization.ColumnChart(document.getElementById('grafico_bar_loads'));
        chart.draw(data, options);
      }
    </script>
    """
    
  
  return estructura

    
def tasks_info():
	tasks_info = subproceso.output_proceso("top | head -2 | tail -1")
	tasks_info = re.sub(" +", " ", tasks_info)
	tasks_info = tasks_info.split(" ")

	names = ["Ejecutando","Hibernando", "Detenidas", "Zombie"]
	tasks_info = [float(tasks_info[3]),float(tasks_info[6]), float(tasks_info[9]), float(tasks_info[12])]

	return graficos.generar_grafico_pastel("pastel_tasks_info","Tareas en el Sistema",names,tasks_info)


processors_load()  
      
    
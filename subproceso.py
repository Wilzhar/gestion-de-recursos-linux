import subprocess, os

def output_proceso(bash_command):
	"""Metodo que ejecuta un comando de bash con os

		Parameters
		----------
		bash_commnad : str
			instrucción del bash (Bourne-again shell) 

		Returns
		-------
		str
			salida del comando bash 
	"""
	return os.popen( bash_command ).read().strip()

def ejecutar_subproceso(bash_command):
	"""Metodo que ejecuta un comando de bash en un subproceso

		Parameters
		----------
		bash_commnad : str
			instrucción del bash (Bourne-again shell) 

		Returns
		-------
		subprocess.Popen
			resultados del comando de bash


	"""

	p = subprocess.Popen(bash_command, 
							stdout = subprocess.PIPE, 
							stderr = subprocess.PIPE,
							shell = True
						)
	p.wait()
	
	
	lines = list()
	for line in p.stdout.readlines():
		lines.append(line.decode().strip())
	return lines, p.stderr.read().decode()

#ejecutar_subproceso("df -h | grep '/dev/'")

#! /usr/bin/python3
#-*- coding:utf-8 -*- 

import subproceso, os, sys,  graficos

#SE OBTIENE LA INFO BASICA DEL SISTEMA
def info_basica():
	"""Metodo que ejecuta un comando de bash en un subproceso

		Returns
		-------
		str[]
			resultados de info basica
	"""
	infoBasica = [
	    ["Nombre del kernel",subproceso.output_proceso ( "uname -s" )],
	    ["Nombre del pc",subproceso.output_proceso ( "uname -n" )],
	    ["Lanzamiento del kernel",subproceso.output_proceso ( "uname -r" )],
	    ["Versión del kernel",subproceso.output_proceso ( "uname -v" )],
	    ["Sistema operativo",subproceso.output_proceso ( "uname -o" )],
	    ["Dirección MAC",subproceso.output_proceso ( "ifconfig | grep ether" ).split()[1]]
	]
	infoBasicaHTML = obtenerCodigoHTMLFilas(infoBasica)

	html = \
	"""<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	   <script type="text/javascript">
	  google.charts.load('current', {'packages':['table']});
	  google.charts.setOnLoadCallback(drawTable);

	  function drawTable() {
	    var data = new google.visualization.DataTable();
	    data.addColumn('string', 'Caracteristica');
	    data.addColumn('string', 'Valor');
	    data.addRows("""+infoBasicaHTML+""");

	    var table = new google.visualization.Table(document.getElementById('tabla_info_basica'));

	    table.draw(data, {showRowNumber: true, width: '100%', height: '100%'});
	  }
    </script>"""
	return html

#SE OBTIENEN LOS DATOS DE MEMORIA CON "free" -----------------
def info_memoria():
	"""Metodo que ejecuta un comando de bash con os

		Returns
		-------
		float
			porcentaje de uso de la ram y de la swap 
	"""
	lineas,_ = subproceso.ejecutar_subproceso("free -m")

    #MEMORIA RAM A PARTIR DE LA SALIDA DEL COMANDO "free" --------
	infoMemoria = lineas[1].split() #SE DIVIDE LA CADENA POR ESPACIOS (SIN IMPORTAR SU TAMAÑO)

	totalMemoria = infoMemoria [ 1 ]
	usoMemoria = infoMemoria [ 2 ]
	porcentajeRam = float (usoMemoria) / float(totalMemoria) * 100.0 #USO/TOTAL PARA HALLAR EL PORCENTAJE

    #MEMORIA SWAP A PARTIR DE LA SALIDA DEL COMANDO "free" --------
	infoMemoriaSwap = lineas[2].split() #SE DIVIDE LA CADENA POR ESPACIOS (SIN IMPORTAR SU TAMAÑO)

	totalMemoriaSwap = infoMemoriaSwap [ 1 ]
	usoMemoriaSwap = infoMemoriaSwap [ 2 ]
	porcentajeSwap = float (usoMemoriaSwap) / float(totalMemoriaSwap) * 100.0 #USO/TOTAL PARA HALLAR EL PORCENTAJE

	html = """
	<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
	<script type="text/javascript">
    google.charts.load('current', {packages: ['corechart', 'bar']});
    google.charts.setOnLoadCallback(drawAnnotations);

    function drawAnnotations() {

      var data = google.visualization.arrayToDataTable([
        ['porcentaje', 'Ocupado', 'Libre', { role: 'style' } ],
        ['Memoria ram', """+str(porcentajeRam)+", "+str(porcentajeRam)+""", ''],
        ['Memoria swap', """+str(porcentajeSwap)+", "+str(porcentajeSwap)+""", '']]);
    
        var options = {
            isStacked: 'percent',
            height: 300,
            legend: {position: 'top', maxLines: 3},
            vAxis: {
              minValue: 0,
              ticks: [0, .3, .6, .9, 1]
            }
          };

        var chart = new google.visualization.ColumnChart(document.getElementById('bar_memoria'));
        chart.draw(data, options);
      }
    </script>
	"""
	return html
#CARGA CPU A PARTIR DE LA SALIDA DEL COMANDO "top" -------------
def info_cpu():
	"""Metodo que ejecuta un comando de bash con os

		Returns
		-------
		float
			porcentaje de uso de la cpu 
	"""
	lineas,_ = subproceso.ejecutar_subproceso( "top -n1" )

	#EL PORCENTAJE QUE SE ENCUENTRA EN LA TERCERA LINEA DE LA SALIDA DE "top"
	infoCPU = lineas[2].split() #SE DIVIDE LA CADENA POR ESPACIOS (SIN IMPORTAR SU TAMAÑO)
	porcentajeCPU = infoCPU [ 1 ] 
	porcentajeCPU = porcentajeCPU.replace(",", ".")

	names = ['Usado', 'Libre']
	porcentajeCPU = float(porcentajeCPU)

	datos_cpu = [porcentajeCPU, 100-porcentajeCPU]

	html = graficos.generar_grafico_pastel("pie_chart_3", "Uso de memoria (MB)", names, datos_cpu)
	return html

#METODO PARA A PARTIR DE UNA MATRIZ OBTENER LAS FILAS PARA AGREGAR A UNA TABLA HTML
def obtenerCodigoHTMLFilas(matriz):	
	filasHTML = "["
	for x in matriz:
		filasHTML += "["
		filasHTML += str(f'\'{x[0]}\'') +","+ str(f'\'{x[1]}\'')
		filasHTML += "],"
	filasHTML = filasHTML[:-1]
	filasHTML += "]"
	return filasHTML

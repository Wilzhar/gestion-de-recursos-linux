def generar_grafico_pastel(id, title, names, datos):
	"""Metodo que me grafica los datos en un grafico de pastel

		Returns
		-------
		str
			html de la grafica


	"""
	estructura =  """
    
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = google.visualization.arrayToDataTable([
			    ['name', 'value'], \n"""
	
	for i in range(len(datos)):
		if i == len(datos) - 1:
			estructura += f"          ['{names[i]}', {datos[i]}] \n"
		else:
			estructura += f"          ['{names[i]}', {datos[i]}], \n"

	estructura += """
	     ]);

        var options = {\n"""
	estructura += f"          title: '{title}',"
	estructura += """
          is3D: true,
        };"""


	estructura += f"var chart = new google.visualization.PieChart(document.getElementById('{id}'));"
	estructura += """
        chart.draw(data, options);
      }
    </script>
	"""

	return estructura
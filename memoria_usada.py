import subproceso
import re
import sys
import graficos


def calcular_uso_memoria_ram():	
	""" Metodo que me calcula el porcentaje de memoria RAM usada

		Returns
		-------
		float
			porcentaje de memoria RAM usada
	"""
	p = subproceso.ejecutar_subproceso("free -m")
	info_memoria = p[0][1]
	info_memoria = re.sub(" +", " ", info_memoria)
	campos_memoria = info_memoria.split(" ") 
	#[uso de memoria, memoria libre, cache y buffer]
	names = ["Usada", "Libre", "Cache y buffer"]
	datos_memoria = [campos_memoria[2], campos_memoria[3], campos_memoria[5]]

	return graficos.generar_grafico_pastel("pie_chart_1", "Uso de memoria (MB)", names, datos_memoria)

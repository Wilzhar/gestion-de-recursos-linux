import subproceso
import re
import sys

def graficar_tiempo_uso_cpu():
	"""Metodo que me grafica la informacion del uso de la memoria

		Returns
		-------
		str
			html de la grafica


	"""
	estructura =  """
    
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {'packages':['gauge']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {

            var data = google.visualization.arrayToDataTable([
            ['Label', 'Value'],
            ['Memory', %s]
            ]);

            var options = {
            width: 400, height: 120,
            redFrom: 90, redTo: 100,
            yellowFrom:75, yellowTo: 90,
            minorTicks: 5
            };

            var chart = new google.visualization.Gauge(document.getElementById('chart_div'));

            chart.draw(data, options);
        }
        </script>
   """
	porcentaje = str(calcular_uso_memoria_ram())
	return estructura % (porcentaje)


def calcular_uso_memoria_ram():	
	""" Metodo que me calcula el porcentaje de memoria RAM usada

		Returns
		-------
		float
			porcentaje de memoria RAM usada
	"""
	p = subproceso.ejecutar_subproceso("free -m")
	info_memoria = p[0][1]
	info_memoria = re.sub(" +", " ", info_memoria)
	campos_memoria = info_memoria.split(" ") 
	total_memoria = campos_memoria[1]
	uso_memoria = campos_memoria[2]

	porcentaje = float(float(uso_memoria) / float(total_memoria)) * 100.0
	porcentaje = round(porcentaje, 2)
	return porcentaje